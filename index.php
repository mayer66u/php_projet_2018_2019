<?php
/**
 * @author Leclerc Oscar, Kircher Nicolas, Martignon Thomas et Mayer Théo
 * PHP - Projet MyWishList 
 */
require 'vendor/autoload.php';

session_start();
use wishlist\models\Liste;

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();

$app->get('/', function () use ($app) {
    $app->redirect($app->urlFor('touteslistes'));
});

/**
 * Affiche toutes les listes
 */
$app->get('/listes', function () {
    $c = new \wishlist\controleurs\ControlListe();
    $c->afficherToutesLesListes();
})
    ->name('touteslistes');

/**
 * Affiche une liste en fonction de son ID
 */

$recupID;
$app->get('/liste/:id', function ($id) use ($app) {
    $verif = Liste::find($id);
    if (isset($_SESSION['iduser'])) {
        if ($_SESSION['iduser'] == $verif['user_id'] || $verif['publique'] == 1) {
            $_SESSION['idL'] = $id;
            $c = new \wishlist\controleurs\ControlListe();
            $c->afficherListe($id);
        } else {
            \wishlist\vues\VueErreur::render('Erreur: Vous n\'avez pas accès à cette page');
            $app->redirect($app->urlFor('touteslistes'));
        }
    } else {
        if ($verif['publique'] == 1) {
            $_SESSION['idL'] = $id;
            $c = new \wishlist\controleurs\ControlListe();
            $c->afficherListe($id);
        } else {
            \wishlist\vues\VueErreur::render('Erreur: Vous n\'avez pas accès à cette page');
            $app->redirect($app->urlFor('touteslistes'));
        }
    }
})
    ->name('listeID');

$app->get('/partage/:tok', function ($tok) use ($app) {
    $id = Liste::where('token_partage', '=', $tok)->first();
    $_SESSION['tok'] = $id['token_partage'];
    $_SESSION['idL'] = $id['no'];
    $c = new \wishlist\controleurs\ControlListe();
    $c->afficherPartageListe($tok);
})
    ->name('listePartage');
/**
 * Affiche l'item à id de la liste idListe
 */
$app->get('/liste/:idListe/:id', function ($idListe, $id) use ($app) {
    $c = new \wishlist\controleurs\ControlItem();
    $c->afficherUnItem($idListe, $id);
});

$app->get('/partage/:tok/:id', function ($tok, $id) {
    $c = new \wishlist\controleurs\ControlItem();
    $c->afficherUnItemP($tok, $id);
})
    ->name('listePart');

/**
 * Affiche le formulaire de la création d'un item pour la liste idListe
 */
$app->get('/formuCreerItemPourListe', function () {
    $c = new \wishlist\controleurs\ControlItem();
    $c->afficherFormuCreationItem();
});

/**
 * Création d'un item pour une liste
 */
$app->post('/creerItemPourListe', function () use ($app) {
    $c = new \wishlist\controleurs\ControlItem();
    $c->creationItemPourListe($_SESSION['idL']);
    $app->redirect($app->urlFor('listeID', array(
        'id' => $_SESSION['idL']
    )));
});

/**
 * Suppression d'un item pour une liste
 */
$app->get('/suppressionItemPourListe/:id', function ($id) use ($app) {
    $c = new \wishlist\controleurs\ControlItem();
    $c->suppressionItemPourListe($id);
    $app->redirect($app->urlFor('listeID', array(
        'id' => $_SESSION['idL']
    )));
});

/**
 * Suppression de l'image d'un item donné
 */
$app->get('/suppressionImageItem/:id', function ($id) use ($app) {
    $c = new \wishlist\controleurs\ControlItem();
    $c->suppressionImageItem($id);
    $app->redirect($app->urlFor('listeID', array(
        'id' => $_SESSION['idL']
    )));
});
/**
 * Modification d'un item pour une liste
 */
$app->post('/modifierItemPourListe/:id', function ($id) use ($app) {
    $c = new \wishlist\controleurs\ControlItem();
    $c->modifierItemPourListe($id);
    $app->redirect($app->urlFor('listeID', array(
        'id' => $_SESSION['idL']
    )));
});

/**
 * Affiche le formulaire de la modification d'un item pour la liste idListe
 */
$app->get('/formuModifItemPourListe/:id', function ($id) {
    $c = new \wishlist\controleurs\ControlItem();
    $c->afficherFormuModificationItem($id);
});

/**
 * Affiche le formulaire de la création d'une liste
 */
$app->get('/formuCreerListe', function () {
    $c = new \wishlist\controleurs\ControlListe();
    $c->afficherFormuCreationListe();
});

/**
 * Création de la liste dans la base de données
 */
$app->post('/creerNouvListe', function () use ($app) {
    $c = new \wishlist\controleurs\ControlListe();
    $c->creationListe();
    $app->redirect($app->urlFor('touteslistes'));
});

/**
 * Acces a la page de connection
 */
$app->get('/connection', function () {
    $c = new \wishlist\controleurs\ControlCompte();
    $c->afficherConnection();
});

/**
 * Test la connection
 */
$app->post('/connectionUser', function () use ($app) {
    $c = new \wishlist\controleurs\ControlCompte();
    $r = $c->connection();
    if ($r == 0) {
        $app->redirect($app->urlFor('touteslistes'));
    } else {
        $erreur = "<h3>Erreur de connexion, identifiant ou mot de passe incorrecte.</h3> <button type=\"text\"><a href=\"connection\">Réessayer la connexion</a></button>";
        \wishlist\vues\VueErreur::render($erreur);
    }
});

$app->get('/inscription', function () {
    $c = new \wishlist\controleurs\ControlCompte();
    $c->afficherInscription();
});

$app->post('/inscriptionUser', function () use ($app) {
    $c = new \wishlist\controleurs\ControlCompte();
    $r = $c->inscription();
    if ($r == 0) {
        $app->redirect($app->urlFor('touteslistes'));
    } else {
        $erreur = "<h3>Erreur d'authentification, information(s) non valide(s).</h3> <button type=\"text\"><a href=\"inscription\">Réessayer l'inscription</a></button>";
        \wishlist\vues\VueErreur::render($erreur);
    }
});

$app->get('/connecter', function () {
    $c = new \wishlist\controleurs\ControlCompte();
    $c->estConnecter();
})
    ->name('connecter');

$app->get('/deconnection', function () use ($app) {
    $c = new \wishlist\controleurs\ControlCompte();
    $c->deconnection();
    $app->redirect($app->urlFor('touteslistes'));
});

$app->get('/lesCreateurs', function () {
    $c = new \wishlist\controleurs\ControlListe();
    $c->afficherCreateurs();
});

$app->get('/suppCompte/:id', function ($id) use ($app) {
    if (isset($_SESSION['iduser']) && $_SESSION['iduser'] == $id) {
        $c = new \wishlist\controleurs\ControlCompte();
        $c->supprimerCompte($id);
        $app->redirect($app->urlFor('touteslistes'));
    } else {
        $erreur = "<h3>Erreur d'accès, vous devez être authentifié pour accéder à cette page.</h3><br />
            <button type=\"text\"><a href=\"inscription\">S'inscrire</a></button>
            <button type=\"text\"><a href=\"connection\">Se connecter</a></button>";
        \wishlist\vues\VueErreur::render($erreur);
    }
});

$app->get('/formuReserverItem/:id', function ($id) use ($app) {
    $c = new \wishlist\controleurs\ControlReservation();
    $c->formuleReserverItem($id);
});

$app->post('/reserverItem/:id', function ($id) use ($app) {
    $c = new \wishlist\controleurs\ControlReservation();
    $c->reserverItem($id);
    if (! isset($_SESSION['tok'])) {
        $app->redirect($app->urlFor('listeID', array(
            'id' => $_SESSION['idL']
        )));
    } else {
        $app->redirect($app->urlFor('listePartage', array(
            'tok' => $_SESSION['tok']
        )));
    }
});

$app->post('/creeCommentaire/:id', function ($id) use ($app) {
    $c = new \wishlist\controleurs\ControlCommentaire();
    $c->creeComm($id);
    if (! isset($_SESSION['tok'])) {
        $app->redirect($app->urlFor('listeID', array(
            'id' => $_SESSION['idL']
        )));
    } else {
        $app->redirect($app->urlFor('listePartage', array(
            'tok' => $_SESSION['tok']
        )));
    }
});

$app->get('/afficheProfil', function () {
    if (isset($_SESSION['iduser'])) {
        $c = new \wishlist\controleurs\ControlCompte();
        $c->afficherProfil();
    } else {
        $erreur = "<h3>Erreur d'accès, vous devez être authentifié pour accéder à cette page.</h3><br />
            <button type=\"text\"><a href=\"inscription\">S'inscrire</a></button>
            <button type=\"text\"><a href=\"connection\">Se connecter</a></button>";
        \wishlist\vues\VueErreur::render($erreur);
    }
});

$app->get('/modifMdp', function () {
    if (isset($_SESSION['iduser'])) {
        $c = new \wishlist\controleurs\ControlCompte();
        $c->afficherFormuMDP();
    } else {
        $erreur = "<h3>Erreur d'accès, vous devez être authentifié pour accéder à cette page.</h3><br /> 
            <button type=\"text\"><a href=\"inscription\">S'inscrire</a></button>
            <button type=\"text\"><a href=\"connection\">Se connecter</a></button>";
        \wishlist\vues\VueErreur::render($erreur);
    }
});

$app->post('/modifMdp', function () use ($app) {
    $c = new \wishlist\controleurs\ControlCompte();
    $c->modifierMdp();
    $app->redirect($app->urlFor('touteslistes'));
});

$app->post('/liste/:idListe/:id/creeParticipe/', function ($idListe, $id) use ($app) {
    $c = new \wishlist\controleurs\ControlCagnotte();
    $r = $c->addParticipe($id);
    if ($r == 1) {
        $app->redirect($app->urlFor('listeID', array(
            'id' => $_SESSION['idL']
        )));
    } else {
        \wishlist\vues\VueErreur::render('Erreur cagnotte depassé');
    }
});

$app->post('/partage/:tok/:id/creeParticipe/', function ($tok, $id) use ($app) {
    $c = new \wishlist\controleurs\ControlCagnotte();
    $r = $c->addParticipe($id);
    if ($r == 1) {
        $app->redirect($app->urlFor('listePartage', array(
            'tok' => $_SESSION['tok']
        )));
    } else {
        \wishlist\vues\VueErreur::render('Erreur cagnotte depassé');
    }
});

$app->get('/partagerListe/:id', function ($id) {
    $_SESSION['partageL'] = 1;
    $c = new \wishlist\controleurs\ControlListe();
    $c->afficherListe($id);
    unset($_SESSION['partageL']);
});

$app->run();
