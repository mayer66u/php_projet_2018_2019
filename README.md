# README #

Membres du groupe : 
* LECLERC Oscar
* KIRCHER Nicolas
* MARTIGNON Thomas
* MAYER Th�o
S3B

Lien du site : http://mywishlist1819.herokuapp.com

Tutoriel d'installation sur une machine :
	
	1. Clonez le projet sur votre machine :
		Copiez le lien suivant : git clone git@bitbucket.org:mayer66u/php_projet_2018_2019.git
		Collez le dans un terminal � l'endroit que vous souhaitez et faites entrer.
		(cela peut prendre un peu de temps, il faut que les fichiers se copient sur votre machine)
	2. Faites un "cd php_projet_2018_2019" dans votre terminal
	3. Il faut installer composer, pour cela taper
		A) Si vous �tes sur MacOS : "php composer.phar install"
		B) Si vous �tes sur Windows : "composer install"
	4. Vous devez disposer d'un server local (XAMP, MAMP, WAMP...), lancez-le.
	5. Ouvrez votre navigateur et acc�der � votre base de donn�es local (http://localhost:8888/phpMyAdmin sur mac ou localhost/phpMyAdmin sur windows)
	6. Cr�ez une nouvelle base donn�es nomm�e "mywishlist"
	7. Dans le r�pertoire php_projet_2018_2019, ouvrez le fichier base_de_donnees.txt et collez son contenu dans la console SQL 
	de votre base mywishlist et ex�cutez-le.
	8. Une fois cette op�ration termin�e, vous pouvez ouvrir une nouvelle fen�tre dans votre navigateur et vous rendre � votre
	adresse locale pour utiliser l'application web.