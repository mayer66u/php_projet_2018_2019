<?php
/**
 * @author Leclerc Oscar, Kircher Nicolas, Martignon Thomas et Mayer Théo
 * Projet PHP - My WishList 
 */
namespace wishlist\models;

class Liste extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'liste';
    protected $primaryKey = 'no';
    public $timestamps = false;

    public function items()
    {
        return $this->hasMany('\wishlist\models\Item', 'liste_id');
    }

    public function commentaires(){
        return $this->hasMany('\wishlist\models\Commentaire','id_list');
    }
}
