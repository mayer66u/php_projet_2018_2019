<?php
/**
 * @author Leclerc Oscar, Kircher Nicolas, Martignon Thomas et Mayer Théo
 * Projet PHP - My WishList 
 */
namespace wishlist\models;

class User extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'User';
    protected $primaryKey = 'iduser';
    public $timestamps = false;
    
    public function listes()
    {
        return $this->hasMany('\wishlist\models\Liste', 'user_id');
    }

    public function commentaires(){
        return $this->hasMany('\wishlist\models\Commentaire', 'user_id');
    }

    public function participe(){
        return $this->hasMany('\wishlist\models\Participe', 'id_user');
    }
}
