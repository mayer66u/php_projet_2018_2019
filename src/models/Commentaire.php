<?php

/**
 * @author Leclerc Oscar, Kircher Nicolas, Martignon Thomas et Mayer Théo
 * PHP - Projet MyWishList 
 */

namespace wishlist\models;


class Commentaire extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'commentaire';
    protected $primaryKey = 'id_comm';
    public $timestamps = false;

    public function liste(){
        return $this->belongsTo('\wishlist\models\Liste','id_list');
    }
}
