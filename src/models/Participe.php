<?php

/**
 * @author Leclerc Oscar, Kircher Nicolas, Martignon Thomas et Mayer Théo
 * PHP - Projet MyWishList 
 */

namespace wishlist\models;


class Participe extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'participe';
    protected $primaryKey = 'id_part';
    public $timestamps = false;

}
