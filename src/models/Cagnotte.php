<?php

/**
 * @author Leclerc Oscar, Kircher Nicolas, Martignon Thomas et Mayer Théo
 * PHP - Projet MyWishList 
 */

namespace wishlist\models;


class Cagnotte extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'cagnotte';
    protected $primaryKey = 'id_cagn';
    public $timestamps = false;

    public function participes(){
        return $this->hasMany('\wishlist\models\Participe','id_cagn');
    }
}
