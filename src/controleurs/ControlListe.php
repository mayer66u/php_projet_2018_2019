<?php
/**
 * @author Leclerc Oscar, Kircher Nicolas, Martignon Thomas et Mayer Théo
 * Projet PHP - My WishList  
 */
namespace wishlist\controleurs;

use wishlist\models\Liste;
use wishlist\vues\VueListes;

class ControlListe
{

    /**
     * Permet d'afficher l'ensemble des listes de la base
     */
    public function afficherToutesLesListes()
    {
        unset($_SESSION['idL']);
        $listl = Liste::all();
        $vue = new VueListes($listl->toArray());
        $vue->render(1);
    }

    /**
     * Affiche la liste qui a cet ID
     *
     * @param int $id
     */
    public function afficherListe($id)
    {
        $liste = Liste::find($id);
        
        // test si la date d'expiration est dépassé ou non
        $l = $liste->toArray();
        
        if (strtotime($l['expiration']) - strtotime("now") < 0) {}
        $obj = $liste->items;
        
        $com = $liste->commentaires;
        
        $vue = new VueListes($obj->toArray(), $com->toArray());
        $vue->render(2);
    }

    /**
     * Affiche le formulaire de la création d'une liste
     */
    public function afficherFormuCreationListe()
    {
        $listl = Liste::all();
        $vue = new VueListes($listl->toArray());
        $vue->render(5);
    }

    /**
     * Créer une liste dans la base
     */
    public function creationListe()
    {
        $app = \Slim\Slim::getInstance();
        $publi = $app->request->post('publique');
        $titre = filter_var($app->request->post('titre'), FILTER_SANITIZE_SPECIAL_CHARS);
        $descr = filter_var($app->request->post('descr'), FILTER_SANITIZE_SPECIAL_CHARS);
        $date = $app->request->post('expiration');
        $token  =  bin2hex ( random_bytes ( 8 ) ) ;
        $token_partage = bin2hex( (random_bytes(8)));
        if (! empty($titre) and ! empty($descr)) { // and ! empty($date)
            $l = new Liste();
            $l->publique = $publi;
            $l->titre = $titre;
            $l->description = $descr;
            $l->expiration = $date;
            $l->token = $token;
            $l->token_partage = $token_partage;
            if (isset($_SESSION['iduser'])) {
                $l->user_id = $_SESSION['iduser'];
            }
            // generer token
            $l->save();
            $p = 'createurListe' . $l['no'];
            setcookie($p, 0, strtotime($date));
        } else
            echo "Un ou plusieurs champs n'est pas correct";
    }

    /**
     * Afficher la listes des créateurs
     */
    public function afficherCreateurs()
    {
        $listl = Liste::distinct()->get(['user_id']);
        $vue = new VueListes($listl);
        $vue->render(8);
    }

    /**
     * Affiche le lien pour le partage
     */
    public function afficherPartageListe($t)
    
    {
        $liste = Liste::where('token_partage', '=', $t)->first();
       
        
        if (strtotime($liste['expiration']) - strtotime("now") < 0) {}
        $obj = $liste->items;
        
        $com = $liste->commentaires;
        
        $vue = new VueListes($obj->toArray(), $com->toArray());
        $vue->render(2);
    }
}
