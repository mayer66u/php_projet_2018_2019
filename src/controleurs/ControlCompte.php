<?php
/**
 * @author Leclerc Oscar, Kircher Nicolas, Martignon Thomas et Mayer Théo
 * Projet PHP - My WishList   
 */
namespace wishlist\controleurs;

use wishlist\authentification\Authentication;
use wishlist\models\User;
use wishlist\vues\VueCompte;
use wishlist\vues\VueErreur;

class ControlCompte
{

    public function afficherConnection()
    {
        $v = new VueCompte();
        $v->render(1);
    }

    /**
     * @return number l'état de la connection
     */
    public function connection()
    {
        $app = \Slim\Slim::getInstance();
        $id = $app->request->post('identifiant');
        $mdp = $app->request->post('mdp');


        $u = User::where('username', '=', $id)->first();
        if (isset($u)) {
            $r = Authentication::authenticate($id, $mdp);
        }else{
            $r = -1;
        }
        return $r;
    }

    /**
     * Affiche le formulaire de l'inscription
     */
    public function afficherInscription()
    {
        $v = new VueCompte();
        $v->render(2);
    }

    public function inscription()
    {
        $app = \Slim\Slim::getInstance();
        $titre = filter_var($app->request->post('titre'), FILTER_SANITIZE_SPECIAL_CHARS);
        $ident = filter_var($app->request->post('identifiant'), FILTER_SANITIZE_SPECIAL_CHARS);
        $mdp = $app->request->post('mdp');
        $nom = filter_var($app->request->post('nom'), FILTER_SANITIZE_SPECIAL_CHARS);
        $prenom = filter_var($app->request->post('prenom'), FILTER_SANITIZE_SPECIAL_CHARS);
        $sexe = $app->request->post('sexe');
        $dateN = $app->request->post('dateN');
        $email = filter_var($app->request->post('email'), FILTER_SANITIZE_EMAIL);
        $ville = filter_var($app->request->post('ville'), FILTER_SANITIZE_SPECIAL_CHARS);
        
        if (! empty($ident) and ! empty($mdp) and ! empty($nom) and ! empty($prenom) and ! empty($sexe) and ! empty($dateN) and ! empty($email) and ! empty($ville)) {
            
            Authentication::createUser($ident, $mdp,$nom,$prenom,$sexe,$dateN,$email,$ville);
        }
    }

    public function estConnecter()
    {
        $v = new VueCompte();
        $v->render(3);
    }

    public function deconnection()
    {
        unset($_SESSION['iduser']);
        unset($_SESSION['role']);
    }
    
    /**
     * Affiche le profil de la personne connectée
     */
    public function afficherProfil() {
        $v = new VueCompte();
        $v->render(4);
    }
    
    /**
     * Afficher le formulaire pour modifier le mot de passe
     */
    public function afficherFormuMDP() {
        $v = new VueCompte();
        $v->render(5);
    }
    
    /**
     * Modifie le mot de passe
     */
    public function modifierMdp() {
        $app = \Slim\Slim::getInstance();
        $u = User::find($_SESSION['iduser']);
        $u->passwd = password_hash($app->request->post('mdp'), PASSWORD_DEFAULT, ["cost" => 12]);
        $u->save();
        $this->deconnection();
    }

    /**
     * Supprimer le compte numéro $id
     * @param int $id du user à supprimer
     */
    public function supprimerCompte($id)
    {
        $u = User::find($id);
        $l = $u->listes;
        foreach ($l as $liste) {
            $i = $liste->items;
            foreach ($i as $item) {
                $item->delete();
            }
            $c = $liste->commentaires;
            foreach ($c as $comm) {
                $comm->delete();
            }
            $liste->delete();   
        }    
        $u->delete();
        $this->deconnection();
    }
}