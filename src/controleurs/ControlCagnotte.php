<?php
/**
 * @author Leclerc Oscar, Kircher Nicolas, Martignon Thomas et Mayer Théo
 * Projet PHP - My WishList  
 */
namespace wishlist\controleurs;

use wishlist\authentification\Authentication;
use wishlist\models\Cagnotte;
use wishlist\models\Item;
use wishlist\models\Participe;
use wishlist\models\User;
use wishlist\vues\VueCompte;

class ControlCagnotte
{

    public static function creeCagnotte($idItem){

        $item = Item::find($idItem);

		$prix = $item->tarif;
		
        $c = new Cagnotte();
        $c->prixT = $prix;
        $c->prixR = $prix;
        $c->save();

        $item->cagnotte_id = $c->id_cagn;
        $item->save();
		return $c->id_cagn;
    }

    public static function listParticipe($idItem){
        if (isset(Item::find($idItem)->id_cagn)){
            $cag = Item::find($idItem)->id_cagn;
            $rep = Cagnotte::find($cag)->participes;
        }else{
            $rep = -1;
        }
        return $rep;
    }

    public function peutPayer($idItem, $prix){
        $idCag = Item::find($idItem)->cagnotte_id;
        $prixR = Cagnotte::find($idCag)->prixR;
        if ($prix>$prixR){
            $rep = -1;
        }else{
            $rep = 1;
        }
        return $rep;
    }

    public function addParticipe($idItem){

        $app = \Slim\Slim::getInstance();

        $prix = filter_var($app->request->post('prixP'), FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        $i = Item::find($idItem);
         $t = ControlCagnotte::peutPayer($idItem,$prix);
         if($t ==1 && isset($_SESSION['iduser'])) {
             $c = Cagnotte::find($i->cagnotte_id);
             $c->prixR = $c->prixR - $prix;
             $c->save();
             $p = new Participe();
             $p->id_cagn = $c->id_cagn;
             $p->id_user = $_SESSION['iduser'];
             $p->aPaye = $prix;
             $p->save();
            return 1;
         }else{
            return -1;
         }
     }
 }