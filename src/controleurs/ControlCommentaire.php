<?php
/**
 * @author Leclerc Oscar, Kircher Nicolas, Martignon Thomas et Mayer Théo
 * Projet PHP - My WishList  
 */

namespace wishlist\controleurs;

use wishlist\models\Commentaire;
use wishlist\models\Liste;
use wishlist\vues\VueReservation;
use wishlist\models\Item;

class ControlCommentaire
{
    /**
     * Création d'un commentaire
     * @param int $id
     */
    public function creeComm($id){
        $app = \Slim\Slim::getInstance();
        $nom = filter_var($app->request->post('nom_comm'), FILTER_SANITIZE_SPECIAL_CHARS);
        $comm = filter_var($app->request->post('comm'), FILTER_SANITIZE_SPECIAL_CHARS);

        $c = new Commentaire();

        $c['nom']=$nom;
        $c['text']=$comm;
        $c->id_list = $id;
        $c->save();

    }


}