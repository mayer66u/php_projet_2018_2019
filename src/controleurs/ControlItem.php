<?php
/**
 * @author Leclerc Oscar, Kircher Nicolas, Martignon Thomas et Mayer Théo
 * Projet PHP - My WishList 
 */
namespace wishlist\controleurs;

use wishlist\models\Cagnotte;
use wishlist\models\Item;
use wishlist\models\Liste;
use wishlist\vues\VueItem;
use wishlist\vues\VueListes;

class ControlItem
{

    /*public function afficherItemListe($idListe, $id)
    {
        $liste = Liste::where('no', '=', $idListe)->first();
        $obj = $liste->items()
            ->where('id', '=', $id)
            ->get();
        $vue = new VueListes($obj);
        $vue->render(3);
    }*/

    public function afficherFormuCreationItem()
    {
        $listl = Liste::all();
        $vue = new VueListes($listl->toArray());
        $vue->render(4);
    }

    public function afficherUnItem($idListe, $id){
        $liste = Liste::find($idListe);
        $item = Item::find($id);
        $vue = new VueItem($item);
        $vue->render(1);
    }

    public function afficherUnItemP($tok, $id){
        $liste = Liste::where('token_partage', '=', $tok)->first();
        $item = Item::find($id);
        $vue = new VueItem($item);
        $vue->render(1);
    }


    /**
     * Création d'un item pour une liste donnée 
     * $nom = filter_var($app->request->post('nom_item',FILTER_SANITIZE_SPECIAL_CHARS);
     * @param int $id
     */
    public function creationItemPourListe($id)
    {
        $app = \Slim\Slim::getInstance();
        $nom = filter_var($app->request->post('nom_item', FILTER_SANITIZE_FULL_SPECIAL_CHARS));
        $prix = $app->request->post('prix_item');
        $descr = filter_var($app->request->post('descr'), FILTER_SANITIZE_SPECIAL_CHARS);
		$cagn = $app->request->post('cagnotte');
        
        if (! empty($_FILES['img_item']['name'])) {
            $img = $_FILES['img_item']['name'];
            $desti = getcwd() . "/images/" . $_FILES['img_item']['name'];
            move_uploaded_file($_FILES['img_item']['tmp_name'], $desti);
        } else {
            $img = NULL;
        }
        
        if (! empty($app->request->post('urlExt_item'))) {
            $url = filter_var($app->request->post('urlExt_item'), FILTER_SANITIZE_URL);
        } else {
            $url = NULL;
        }
        
        if (! empty($nom) and ! empty($prix) and ! empty($descr)) {
            $i = new Item();
            $i->liste_id = $id;
            $i->nom = $nom;
            $i->descr = $descr;
            $i->tarif = $prix;
            $i->img = $img;
            $i->url = $url;
            $i->save();
			if (isset($cagn)){
                $cagnotte = ControlCagnotte::creeCagnotte($i->id);
            }else{
                $cagnotte = NULL;
            }
            $i->cagnotte_id = $cagnotte;
            $i->save();
        } else
            echo "Un ou plusieurs champs n'est pas correct";
    }

    /**
     * Suppression item donné pour une liste donnée
     * @param int $id de l'item
     */
    public function suppressionItemPourListe($id)
    {
        $i = Item::find($id);
        $i->delete();
    }
    
    /**
     * Suppression item donné pour une liste donnée
     * @param int $id de l'item
     */
    public function suppressionImageItem($id)
    {
        $i = Item::find($id);
        $i->img = NULL;
        $i->save();
    }
    
    /**
     * Affiche le formulaire de la modification d'un item
     */
    public function afficherFormuModificationItem($id)
    {
        $_SESSION['idItem'] = $id;
        $listl = Liste::all();
        $item = Item::where('id', '=', $id)->first();
        $vue = new VueItem($item);
        $vue->render(2);
        unset($_SESSION['idItem']);
    }
    
    /**
     * Modifie item donné pour une liste donnée
     * @param int $id de l'item
     */
    public function modifierItemPourListe($id)
    {
        $i = Item::find($id);
        $app = \Slim\Slim::getInstance();
        
        if (! empty($app->request->post('nom_item'))) {
            $i->nom = filter_var($app->request->post('nom_item'), FILTER_SANITIZE_SPECIAL_CHARS);
        }
        if (! empty($app->request->post('descr'))) {
            $i->descr = filter_var($app->request->post('descr'), FILTER_SANITIZE_SPECIAL_CHARS);
        }
        if (! empty($app->request->post('prix_item'))) {
            $i->tarif = $app->request->post('prix_item');
            if (isset($i->cagnotte_id)){
                $c = Cagnotte::find($i->cagnotte_id);
                $c->prixR = $app->request->post('prix_item') ;
                $c->prixT = $app->request->post('prix_item');

            }
        }
//         if (! empty($_FILES['img_item']['name'])) {
//             $img = $_FILES['img_item']['name'];
//             $desti = getcwd() . "/images/" . $_FILES['img_item']['name'];
//             move_uploaded_file($_FILES['img_item']['tmp_name'], $desti);
//             $i->img = $img;
//         }
        if (! empty($app->request->post('urlExt_item'))) {
            $i->url = filter_var($app->request->post('urlExt_item'), FILTER_SANITIZE_URL);
        }
		$fC = $app->request->post('Fcagnotte');
        $oC = $app->request->post('Ocagnotte');

        if (isset($fC)){
            $c = Cagnotte::find($i->cagnotte_id);
            $c->delete();
            $i->cagnotte_id = NULL;
        }
        if (isset($oC)){
            ControlCagnotte::creeCagnotte($id);
        }
        $i->save();
    }
    
}