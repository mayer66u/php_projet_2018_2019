<?php
/**
 * @author Leclerc Oscar, Kircher Nicolas, Martignon Thomas et Mayer Théo
 * Projet PHP - My WishList  
 */
namespace wishlist\controleurs;

use wishlist\models\Liste;
use wishlist\vues\VueReservation;
use wishlist\models\Item;

class ControlReservation
{

    public function formuleReserverItem($id)
    {
        $i = Item::find($id);
        
        $v = new VueReservation($i);
        $v->render();
    }

    
    public function reserverItem($id)
    {
        $app = \Slim\Slim::getInstance();
        $nomP = $app->request->post('nom_part');
        $desc = $app->request->post('desc_reserv');
        
        $i = Item::find($id);
        $i['participant'] = $nomP;
        $i['descrPart'] = $desc;
        
        $i->save();
    }
}