<?php
/**
 * @author Leclerc Oscar, Kircher Nicolas, Martignon Thomas et Mayer Théo
 * Projet PHP - My WishList
 */
namespace wishlist\authentification;

use wishlist\models\User;

class Authentication
{

    public static function createUser($userName, $password, $nom, $prenom, $sexe, $dateNaiss, $email, $ville)
    {
        $u = User::where('username', '=', $userName);
        
        if (! isset(User::where('username', '=', $userName)->username)) {
            // test mot de passe et nom
            if (strlen($userName) <= 20 and strlen($password) <= 25) {
                $user = new User();
                $user->username = $userName;
                $user->passwd = password_hash($password, PASSWORD_DEFAULT, ["cost" => 12]);
                $user->role = 1;
                $user->nom = $nom;
                $user->prenom = $prenom;
                $user->sexe = $sexe;
                $user->dateNaiss = $dateNaiss;
                $user->email = $email;
                $user->ville = $ville;
                $user->save();
                $r = Authentication::authenticate($userName, $password);
                return $r;
            }
        }else{
            return -1;
        }
    }

    public static function authenticate($username, $password)
    {
            $u = User::where('username', '=', $username)->first();

            if(password_verify($password, $u->passwd)){
                $_SESSION['connectStatus'] = 'connect';
                Authentication::loadProfile($u->iduser);
                return 0;
            } else {
                $_SESSION['connectStatus'] = 'connectionFail';
                return -1;

            }

    }

    
    public static function loadProfile($uid)
    {
        $u = User::find($uid);
        
        $_SESSION['iduser'] = $u['iduser'];
        $_SESSION['role'] = $u['role'];
    }

    
    public static function checkAccessRights($required)
    {
        return $_SESSION['acces'] >= $required;
    }
}