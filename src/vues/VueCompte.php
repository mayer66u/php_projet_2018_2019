<?php
/**
 * @author Leclerc Oscar, Kircher Nicolas, Martignon Thomas et Mayer Théo
 * Projet PHP - My WishList 
 */
namespace wishlist\vues;

use wishlist\models\User;

class VueCompte
{

    public function afficherConnection()
    {
        $try = '';
        
        if (isset($_SESSION['connectTry'])) {
            if ($_SESSION['connectTry'] == 1) {
                $try = '<p> Identifiant ou mot de passe incorrect </p>';
            }
        }
        
        $c = <<<END
        <div class="connect">
                <h2>CONNEXION</h2>
                <form method="post" action="connectionUser">
                       <span>IDENTIFIANT</span>
                       <input type="text" name="identifiant" required="required"> <br /><br />
                       <span>MOT DE PASSE</span>
                       <input type="password" name="mdp" required="required">
                       <input type="submit" value="Se Connecter">
                </form>
                $try
        </div>
END;
        return $c;
    }

    public function afficherInscription()
    {
        $c = <<<END
                <h2>INSCRIPTION</h2>
                <form method="post" action="inscriptionUser">
                        <span>NOM* </span>
                        <input type="text" name="nom" required="required">
                        <span>PRENOM* </span>
                        <input type="text" name="prenom" required="required">
                        <br /><br />
                        <span>IDENTIFIANT* </span>
                        <input type="text" name="identifiant" required="required">
                        <br /><br />
                        <span>MOT DE PASSE* </span>
                        <input type="password" name="mdp" required="required">
                        <br /><br />
                        <span>SEXE* </span>
                        <input type="radio" name="sexe" value="H"/>
                        <span for="sexe">HOMME</span>
                        <input type="radio" name="sexe" value="F"/>
                        <span for="sexe">FEMME</span>
                        <br /><br />
                        <span>DATE DE NAISSANCE* </span>
                        <input type="date" name="dateN" required="required">
                        <br /><br />
                        <span>EMAIL* </span>
                        <input type="email" name="email" required="required">
                        <br /><br />
                        <span>VILLE* </span>
                        <input type="text" name="ville" required="required">
                        <br /><br />
                        <input type="submit" value="S'inscrire">
                        <p>* champs requis</p>
                </form>
END;
        return $c;
    }

    public function estConnect()
    {
        if (isset($_SESSION['iduser'])) {
            $user = User::find($_SESSION['iduser'])['username'];
        } else {
            $user = 'non connecter';
        }
        
        $c = '<h1> Bienvenue ' . $user . '</h1> <div><a href="listes">Accueil</a></div>';
        
        return $c;
    }

    public function afficherProfil()
    {
        $res = "";
        $user = User::find($_SESSION['iduser']);
        $idUs = $_SESSION['iduser'];
        $usname = $user['username'];
        $nom = $user['nom'];
        $prenom = $user['prenom'];
        $dateN = $user['dateNaiss'];
        $email = $user['email'];
        $ville = $user['ville'];
        $res = $res . "<h2>MON ESPACE PERSONNEL</h2><div class=\"affProfil\">";
        $res = $res . "<h5><b>MES INFORMATIONS PERSONNELLES</b></h5>";
        $res = $res . "<b>NOM : </b>$nom &nbsp &nbsp &nbsp &nbsp <b>PRÉNOM : </b>$prenom<br />";
        $res = $res . "<b>USER NAME : </b>$usname &nbsp &nbsp &nbsp &nbsp";
        $res = $res . "<button type=\"text\" class=\"modifMdp\"> <a href=\"modifMdp\"> Modifier mon mot de passe </a></button> <br />";
        $res = $res . "<b>EMAIL :</b> $email<br />";
        $res = $res . "<b>DATE DE NAISSANCE :</b> $dateN<br />";
        $res = $res . "<b>VILLE :</b> $ville<br />";
        $res = $res . "<br /><button type=\"text\" class=\"buttonCree\"><a href=\"suppCompte/$idUs\">Supprimer mon compte</a></button>";
        return $res . "</div>";
    }

    public function afficherFormuMDP()
    {
        $c = <<<END
            <h2>Modification du mot de passe</h2>
            <form method="post" action="modifMdp">
                <span>NOUVEAU MOT DE PASSE</span>
                <input type="password" name="mdp" required="required">

                <input type="submit" value="Enregistrer">      
            </form>
END;
        return $c;
    }

    public function render($sel)
    {
        
//         $css = "src/vues/fileCSS.css";
//         $lienAccueil = "listes";
//         $lienCreateurs = "lesCreateurs";
//         $lienDeco = "deconnection";
//         $lienNvListe = "formuCreerListe";
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request->getRootUri();
        $css = "$rootUri/src/vues/fileCSS.css";
        $lienAccueil = "$rootUri/listes";
        $lienCreateurs = "$rootUri/lesCreateurs";
        $lienDeco = "$rootUri/deconnection";
        $lienNvListe = "$rootUri/formuCreerListe";
        switch ($sel) {
            case 1:
                $content = $this->afficherConnection();
                break;
            case 2:
                $content = $this->afficherInscription();
                break;
            case 3:
                $content = $this->estConnect();
                break;
            case 4:
                $content = $this->afficherProfil();
                break;
            case 5:
                $content = $this->afficherFormuMDP();
                break;
        }
        
        $connectBouton = "";
        if (isset($_SESSION['iduser'])) {
            $connectBouton = "<li class=\"nav-item\"><a class=\"nav-link\" href=$lienNvListe>NOUVELLE LISTE</a></li><li class=\"nav-item\"><a class=\"nav-link\" href=$lienDeco>DÉCONNEXION</a></li>";
        }
        
        
        $html = <<<END
        <!DOCTYPE html>
            <html lang="fr">
            <head>
                <meta charset="utf-8" />
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script> 
                <link rel="stylesheet" href="$css"> 
                <title>My WishList</title>  
                </head>
    
                <body>
                    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                        <a class="navbar-brand" href="$lienAccueil">MyWishList</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                          <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarCollapse">
                          <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                              <a class="nav-link" href="$lienAccueil">ACCUEIL <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="$lienCreateurs">LES CRÉATEURS</a>
                            </li>
                            $connectBouton
                          </ul>
                        </div>
                      </nav>

                    <div class="corps">
                        $content
                    </div>
        <footer>
                <hr>
                <p>Projet PHP - My WishList </p>
                <p>KIRCHER-LECLERC-MARTIGNON-MAYER</p>
                <p><a href="https://bitbucket.org/mayer66u/php_projet_2018_2019/src/master/" target="_blank">Cliquez pour voir notre dépôt GIT</a></p> 
    		  </footer>
                </body>
            </html>
END;
        
        echo $html;
    }
}