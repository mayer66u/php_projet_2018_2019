<?php
/**
 * @author Leclerc Oscar, Kircher Nicolas, Martignon Thomas et Mayer Théo
 * Projet PHP - My WishList 
 */
namespace wishlist\vues;

use wishlist\models\Item;
use wishlist\models\User;

class VueReservation
{
    private $obj;

    public function __construct($o)
    {
        $this->obj = $o;
    }

    public function afficherFormuReservItem()
    {
        $nomI = $this->obj['nom'];
        $id = $this->obj['id'];

        if (!isset($this->obj['cagnotte_id'])) {

            if (isset($_SESSION['iduser'])) {
                $v = User::find($_SESSION['iduser'])['username'];
            } else {
                $v = '';
            }

            $res = "<h2>RÉSERVATION DE L'ITEM : $nomI</h2><br />
                <div class=\"formulaireNewComm\">
            <form method='post' action='../reserverItem/$id' enctype=\"multipart/form-data\">
                <label for=\"nom_part\"> <b>NOM DU PARTICIPANT :*</b></label>
                <input type=\"text\" name=\"nom_part\" id=\"nom_comm\" value='$v' size=\"30\" maxlength=\"20\" autofocus required>
                
                <label for=\"desc_reserv\"> <b>MESSAGE POUR LE CRÉATEUR :*</b></label>
                <textarea name=\"desc_reserv\" id=\"comm\" rows=\"4\" cols=\"60\" required></textarea>
                <br />
                
                <button type=\"submit\" value=\"Valider\"> Valider</button>
                <p>* champs requis</p>
            </div>
        </form>
      ";
            return $res;
        }else{
            VueErreur::render('C est l erreur');
        }
    }


    public function render()
    {
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request->getRootUri();
        $css = "$rootUri/src/vues/fileCSS.css";
        $lienAccueil = "$rootUri/listes";
        $lienCreateurs = "$rootUri/lesCreateurs";
        $lienDeco = "$rootUri/deconnection";
        $lienNvListe = "$rootUri/formuCreerListe";
        $lienConnection = "$rootUri/connection";
        $lienInscription = "$rootUri/inscription";
        $lienProfil = "$rootUri/afficheProfil";
//         $lienAccueil = "../listes";
//         $lienNvListe = "../formuCreerListe";
//         $lienConnection = "../connection";
//         $lienInscription = "../inscription";
//         $lienCreateurs = "../lesCreateurs";
//         $css = "../src/vues/fileCSS.css";
//         $lienProfil = "../afficheProfil";
        
        $content = $this->afficherFormuReservItem();
        
        
        if (isset($_SESSION['iduser'])) {
//             if (isset($_SESSION['idL'])) {
//                 $lienDeco = "../deconnection";
//             } else {
//                 $lienDeco = "deconnection";
//             }
            $connectBouton = "<li class=\"nav-item\"><a class=\"nav-link\" href=$lienDeco>DÉCONNEXION</a></li>";
            $espacePerso = "<li class=\"nav-item\"><a class=\"nav-link\" href=\"$lienProfil\">MON ESPACE </a></li>";
        } else {
            $connectBouton = "<li class=\"nav-item\"><a class=\"nav-link\" href= $lienConnection>CONNEXION</a></li> <li class=\"nav-item\"> <a class=\"nav-link\" href=$lienInscription>INSCRIPTION</a></li>";
            $espacePerso = "";
        }
        
        $creaListe = '';
        if (isset($_SESSION['iduser'])) {
            $creaListe = "<li class=\"nav-item\"> <a class=\"nav-link\" href=\"$lienNvListe\">NOUVELLE LISTE</a></li>";
        }
        $html = <<<END
        <!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script> 
        <link rel="stylesheet" href="$css">
        <title>My WishList</title>
        <meta name="viewport" content="width=device-width" />
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="$lienAccueil">MyWishList</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="$lienAccueil">ACCUEIL <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="$lienCreateurs">LES CRÉATEURS</a>
                </li>
                $creaListe  
                $espacePerso
                $connectBouton
              </ul>
            </div>
          </nav>
          <div class="corps">
            $content
          </div>
        <footer>
                <hr>
                <p>Projet PHP - My WishList </p>
                <p>KIRCHER-LECLERC-MARTIGNON-MAYER</p>
                <p><a href="https://bitbucket.org/mayer66u/php_projet_2018_2019/src/master/" target="_blank">Cliquez pour voir notre dépôt GIT</a></p> 
    		  </footer>     
    </body>
</html>
END;
        
        echo $html;
    }
}