<?php
/**
 * @author Leclerc Oscar, Kircher Nicolas, Martignon Thomas et Mayer Théo
 * Projet PHP - My WishList 
 */
namespace wishlist\vues;

use wishlist\models\Liste;
use wishlist\models\User;

class VueListes
{

    protected $listes, $comm;

    function __construct($l, $comm = NULL)
    {
        $this->listes = $l;
        $this->comm = $comm;
    }

    /**
     *
     * @return l'affichage de toutes les listes ainsi que le bouton pour en créer une nouvelle
     */
    public function afficherToutesLesListes()
    {
        $res = "";
        
        if (isset($_SESSION['iduser'])) { //
            $u = User::find($_SESSION['iduser']);
            $idUs = $_SESSION['iduser'];
        }
        
        $res = $res . "<h2> Listes publiques :</h2>";
        $l = Liste::where('publique', '=', 1)->first();
        if ($l == NULL) {
            $res = $res . "&nbsp &nbsp Aucune liste publique";
        } else {
            $res = $res . "<div class=\"row\">";
            foreach ($this->listes as $liste) { // Pour toutes les listes
                $pub = $liste['publique'];
                if ($pub == 1) { // Si la liste est publique (colonne publique égale à 1)
                    $id = $liste['no'];
                    $titre = $liste['titre'];
                    $descr = $liste['description'];
                    $date = $liste['expiration'];
                    $id_uti = $liste['user_id'];
                    $uti = User::find($id_uti)['username'];
                    $res = $res . "<div class=\"col-md-3\">
                        <h3>$titre</h3>
                        <p class=\"by\"> Créée par $uti<p>
                        <p class=\"description\"><u><b>Description</u></b> : $descr</p>
                        <p><a class=\"btn-secondary\" href=\"liste/$id\" role=\"button\">Voir la liste</a></p>
                    </div>";
                }
            }
            $res = $res . "</div>";
        }
        
        $res = $res . "<br />";
        if (isset($_SESSION['iduser'])) {
            $res = $res . "<h2> Mes listes :</h2>";
            $lp = Liste::where('user_id', '=', $_SESSION['iduser'])->first();
            if ($lp == NULL) {
                $res = $res . "&nbsp &nbsp Aucune liste";
            } else {
                $res = $res . "<div class=\"row\">";
                foreach ($this->listes as $liste) {
                    if ($_SESSION['iduser'] == $liste['user_id']) {
                        $id = $liste['no'];
                        $titre = $liste['titre'];
                        $descr = $liste['description'];
                        $res = $res . "<div class=\"col-md-3\">
                        <h3>$titre</h3>
                        <p class=\"description\"><u><b>Description</u></b> : $descr</p>
                        <p><a class=\"btn-secondary\" href=\"liste/$id\" role=\"button\">Voir ma liste</a></p>
                    </div>";
                    }
                }
                $res = $res . "</div>";
            }
            
            // $res = $res . "<br /><button type=\"text\" class=\"buttonCree\"><a href=\"formuCreerListe\">Créer Liste</a></button>";
        }
        return $res;
    }

    /**
     *
     * @return l'affichage de tous les items d'une liste
     */
    public function afficherItemsUneListe()
    {
        $res = "<h2> Détail de la liste : </h2>";
        $l = Liste::find($_SESSION['idL']);
        $i = $l['no'];
        $nom = $l['titre'];
        $descr = $l['description'];
        $date = $l['expiration'];
        $tokParta = $l['token_partage'];
        $res = $res . "<b>NOM :</b> $nom <br />
                <b>DESCRIPTION :</b> $descr <br />
                <b>DATE D'EXPIRATION :</b> $date <br />";
        if(isset($_SESSION['partageL'])) {
            $res = $res . "<b> LIEN DE PARTAGE : </b>http://mywishlist1819.herokuapp.com/partage/$tokParta<br />";
        }
        if (isset($_SESSION['iduser'])) {
            if (isset($_COOKIE['createurListe' . $_SESSION['idL']]) && $_SESSION['iduser'] == Liste::find($_SESSION['idL'])->user_id && (strtotime(Liste::find($_SESSION['idL'])['expiration']) - time() > 0)) {
                $res = $res . "<button type=\"text\" class=\"btCreaItem\"> <a href=\"../formuCreerItemPourListe\">Créer item</a></button>"; // Bouton pour créer un item
            }
        }
        if (isset($_COOKIE['createurListe' . $_SESSION['idL']]) && (strtotime(Liste::find($_SESSION['idL'])['expiration']) - time() > 0)) {
            $res = $res . "<button type=\"text\" class=\"btCreaItem\" ><a href=\"../partagerListe/$i\">Partager la liste</a></button>"; // Bouton pour partager une liste    partagerListe/$i
        }
        /* $res=$res."Liste numéro $listes('$id')"; */
        // Test pour afficher la liste en question
        $res = $res . "<div class=\"row\">";
        foreach ($this->listes as $item) {
            $id = $item['id'];
            $idListe = $item['liste_id'];
            $tok = Liste::find($idListe);
            $tok = $tok['token_partage'];
            $nom = $item['nom'];
            $descr = $item['descr'];
            
            $participant = $item['participant'];
            $descrReserv = $item['descrPart'];
            
            if(isset($item['img'])) {
                $img = $item['img'];
            } else {
                $img = "nophoto.png";
            }
            $app = \Slim\Slim::getInstance();
            $rootUri = $app->request->getRootUri();
            $res = $res . "<div class=\"col-md-3\">
                            <img src=\"$rootUri/images/$img\" alt=\"$img\" />
                            <h3>$nom</h3>
                            <p class=\"description\"><u><b>Description</u></b> : $descr</p>
                            <p><a class=\"btn-secondary\" href=\"$rootUri/liste/$tok/$id\" role=\"button\">Voir l'item</a></p>
                           </div>";
        }
        $res = $res . "</div>";
        
        $idCom = 0;
        $res = $res . "<br /><h3>ESPACE COMMENTAIRES </h3>";
        if (isset($this->comm)) {
            foreach ($this->comm as $comm) {
                $nomComm = $comm['nom'];
                $comm = $comm['text'];
                $idCom += 1;
                $res = $res . "<div class=\"affiComm\">";
                $res = $res . "<h4>COMMENTAIRE n°$idCom :</h4>";
                $res = $res . "<p><b>NOM :</b> $nomComm <br /><b>MESSAGE : </b>$comm</p></div>";
            }
        }
        $res = $res . $this->afficherFormComm();
        
        return $res;
    }

    public function afficherFormComm()
    {
        $d = $_SESSION['idL'];
        if (isset($_SESSION['iduser'])) {
            $v = User::find($_SESSION['iduser'])['username'];
        } else {
            $v = '';
        }
        $res = <<<END
            <div class="formulaireNewComm">
                <h4>NOUVEAU COMMENTAIRE :</h4>
                <form method='post' action='../creeCommentaire/$d' enctype="multipart/form-data">
                    <label for="nom_comm"> <b>NOM :*</b></label>
                    <input type="text" name="nom_comm" id="nom_comm" value='$v' size="30" maxlength="20" required>                
                    <br />
                    <label for="comm"> <b>COMMENTAIRE :*</b></label><br />
                    <textarea name="comm" id="comm" rows="4" cols="60" required></textarea><br />
                    <button type="submit" value="Valider"> Valider</button>
                    <p>* champs requis</p>
                </form>
            </div>
END;
        return $res;
    }

    /**
     *
     * @return l'affichage d'un item d'une liste
     */
    public function afficherUnItemUneListe()
    {
        $res = "";
        foreach ($this->listes as $item) {
            $res = $res . "Item num: " . $item['id'] . ", nom est: " . $item['nom'] . ", prix : " . $item['tarif'] . "<br>";
        }
        return $res;
    }

    /**
     * Afficher le formulaire de la création d'une liste
     *
     * @return string le formulaire
     */
    public function afficherFormuCreationListe()
    {
        $res = <<<END
        <div class="formulaireModifItem">
            <h2>CRÉATION D'UNE LISTE</h2>
            <div class="text-input">
            <form method="post" action="creerNouvListe">
                <input type="radio" name="publique" value="0" checked="checked" /> <label for="privee"><b>Privée</b></label>
                <input type="radio" name="publique" value="1"/> <label for="publique"> <b>Publique</b></label>
                <br />
                <br />
                <span ><b>TITRE* :</b></span>
                <input type="text\" id="nom_item" size="30" maxlength="30" name="titre" autofocus>
                <br /><br />
                <span><b>DESCRIPTION* : </b></span><br />
                <textarea id="descr" rows="5" cols="25" name="descr" required></textarea>
                <br />
                <span><b>DATE D'EXPIRATION* :</b></span>
                <input type="date" name="expiration" required>
                <br /><br />
                <button type="submit" value="Valider"> Valider</button>
                <br />* champs requis
                
            </form>
            </div>
        </div>
END;
        return $res;
    }

    /**
     *
     * @return string
     */
    public function afficherFormuCreationItem()
    {
        $res = <<<END
        <div class="formulaireModifItem">
            <h2>CRÉATION D'UN ITEM</h2>
            <div class="text-input">
                <form method="post" action="creerItemPourListe" enctype="multipart/form-data">
                    <label for="nom_item"> <b>NOM DE L'ITEM :*</b></label>
                    <input type="text" name="nom_item" id="nom_item" placeholder="Exemple : Champagne" size="30" maxlength="30" autofocus required>
                    
                    <label for="prix_item"> <b>PRIX :*</b></label>
                    <input type="number" name="prix_item" id="prix_item" placeholder="00.00" cmin="0" step="0.01" required>
                    <br /><br />
                    
                    <label for="desc_item"> <b>DESCRIPTION DE L'ITEM :*</b></label> <br />
                    <textarea name="descr" id="descr" rows="4" cols="60" placeholder="Exemple : Bouteille de champagne + flûtes" required></textarea>
                    <br />

                    <p for="img_item"> <b>IMAGE DE L'ITEM :</b></p>
                    <input type="file" name="img_item" id="img" accept="image/*">
                    <br /><br /><br />
                    
                    <p for="urlExt_item"> <b>URL EXTERNE :</b></p>
                    <input type="url" name="urlExt_item" id="url" size="80" maxlength="1000">
                    <br />
    
					<td>Ouvir une cagnotte </td>
                <td><input type = "checkbox" name =cagnotte></td>
                <br />

                    <button type="submit" value="Valider"> Valider</button>
                    <br />* champs requis
                </form>
            </div>
        </div>
END;
        return $res;
    }

    /**
     * Affiche les créateurs de listes publiques
     *
     * @return string
     */
    public function afficherCreateursListes()
    {
        $res = "Listes des créateurs de listes publiques <br />";
        $i = 1;
        foreach ($this->listes as $liste) {
                $idCrea = $liste['user_id'];
                $v = User::find($idCrea)['username'];
                if (isset($v)) {
                    $res = $res . "Créateur n°$idCrea : $v<br />";
                }
        }
        return $res;
    }
    
    /**
     *
     * @param int $sel
     */
    public function render($sel)
    {
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request->getRootUri();
        $css = "$rootUri/src/vues/fileCSS.css";
        $lienAccueil = "$rootUri/listes";
        $lienCreateurs = "$rootUri/lesCreateurs";
        $lienDeco = "$rootUri/deconnection";
        $lienNvListe = "$rootUri/formuCreerListe";
        $lienConnection = "$rootUri/connection";
        $lienInscription = "$rootUri/inscription";
        $lienProfil = "$rootUri/afficheProfil";
        switch ($sel) {
            case 1:
                $content = $this->afficherToutesLesListes();
                break;
            case 2:
                $content = $this->afficherItemsUneListe();
                break;
            case 3:
                $content = $this->afficherUnItemUneListe();
                break;
            case 4:
                $content = $this->afficherFormuCreationItem();
                break;
            case 5:
                $content = $this->afficherFormuCreationListe();
                break;
            case 7:
                $content = $this->partageListe();
                break;
            case 8:
                $content = $this->afficherCreateursListes();
                break;
        }
        
        if (isset($_SESSION['iduser'])) {
            $connectBouton = "<li class=\"nav-item\"><a class=\"nav-link\" href=$lienDeco>DÉCONNEXION</a></li>";
            $espacePerso = "<li class=\"nav-item\"><a class=\"nav-link\" href=\"$lienProfil\">MON ESPACE </a></li>";
        } else {
            $connectBouton = "<li class=\"nav-item\"><a class=\"nav-link\" href= $lienConnection>CONNEXION</a></li> <li class=\"nav-item\"> <a class=\"nav-link\" href=$lienInscription>INSCRIPTION</a></li>";
            $espacePerso = "";
        }
        
        $creaListe = '';
        if (isset($_SESSION['iduser'])) {
            $creaListe = "<li class=\"nav-item\"> <a class=\"nav-link\" href=\"$lienNvListe\">NOUVELLE LISTE</a></li>";
        }
        
        $html = <<<END
   <!DOCTYPE html>
   <html lang="fr">
   <head>
        <meta charset="utf-8" />       
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script> 
        <link rel="stylesheet" href="$css">
        <title>My WishList</title>
    </head>
    <body>            
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="$lienAccueil">MyWishList</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="$lienAccueil">ACCUEIL <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="$lienCreateurs">LES CRÉATEURS</a>
                </li>
                $creaListe  
                $espacePerso
                $connectBouton
              </ul>
            </div>
          </nav>
          <div class="corps">
            $content
          </div>
	       <footer>
                <hr>
                <p>Projet PHP - My WishList </p>
                <p>KIRCHER-LECLERC-MARTIGNON-MAYER</p>
                <p><a href="https://bitbucket.org/mayer66u/php_projet_2018_2019/src/master/" target="_blank">Cliquez pour voir notre dépôt GIT</a></p>
    		  </footer>
        </div>
    </body>
</html>
END;
        
        echo $html;
    }
}