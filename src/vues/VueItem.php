<?php
/**
 * @author Leclerc Oscar, Kircher Nicolas, Martignon Thomas et Mayer Théo
 * Projet PHP - My WishList 
 */
namespace wishlist\vues;

use wishlist\models\Liste;
use wishlist\models\Participe;
use wishlist\models\User;
use wishlist\models\Item;
use wishlist\models\Cagnotte;

class VueItem
{

    protected $liste, $obj;

    function __construct($o)
    {
        $this->liste = Liste::find($o->liste_id);
        $this->obj = $o;
    }

    /**
     *
     * @return l'affichage de objet
     */
    public function afficherItems()
    {
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request->getRootUri();
        $item = $this->obj;
        $id = $item['id'];
        $idListe = $item['liste_id'];
        $nom = $item['nom'];
        $prix = $item['tarif'];
        $descr = $item['descr'];
        
        // Image de l'item
        if (isset($item['img'])) {
            $img = $item['img'];
        } else {
            $img = "nophoto.png";
        }
        $image = "<img src=\"$rootUri/images/$img\" alt=\"image\"/>";
        
        // Lien extérieur
        if (isset($item['url'])) {
            $lien = $item['url'];
            $textLien = "<a href=$lien target=\"_blank\">$lien</a>";
        } else {
            $textLien = "aucun lien disponible";
        }
        $participant = $item['participant'];
        $descrReserv = $item['descrPart'];
        
        // Affichage
        $res = "<div class=\"affItem\">";
        $res = $res . "<h4>ITEM : $nom </h4><br>";
        $res = $res . "$image";
        $res = $res . "<div class=\"descrItem\">";
        $res = $res . "<p><b>NOM</b> : $nom</p>";
        $res = $res . "<p><b>DESCRIPTION</b> : $descr</p>";
        $res = $res . "<p><b>PRIX</b> : $prix €</p>";
        $res = $res . "<p><b>LIEN EXTERNE</b> : $textLien</p>";
        $res = $res . "<b>ÉTAT</b> : ";
        
        // si est createur + avant echeance
        if (isset($_COOKIE['createurListe' . $this->liste['no']]) && (strtotime(Liste::find($_SESSION['idL'])['expiration']) - time() > 0)) {
            if (isset($item['participant'])) {
                $res = $res . "réservé<br />";
            }
            if (!isset($item['participant']) && ! isset($item['cagnotte_id'])){
                $res = $res . "non réservé<br />";
                if($_SESSION['iduser'] == Liste::find($_SESSION['idL'])->user_id) {
                    $res = $res . "<button type=\"text\"> <a href=\"../../formuModifItemPourListe/$id\">Modifier item</a></button>";
                    $res = $res . "<button type=\"text\"> <a href=\"../../suppressionItemPourListe/$id\">Supprimer item</a></button>";
                }
            }
            if (! isset($item['participant'])&& isset($item['cagnotte_id'])){
                $c = Cagnotte::find($item['cagnotte_id']);
                if ($c->prixR > 0){
                    $res = $res . "cagnotte en cours<br />";
                    if($c->prixR == $c->prixT) {
                        $res = $res . "<button type=\"text\"> <a href=\"../../formuModifItemPourListe/$id\">Modifier item</a></button>";
                        $res = $res . "<button type=\"text\"> <a href=\"../../suppressionItemPourListe/$id\">Supprimer item</a></button>";
                    }
                }else{
                    $res = $res . "cagnotte terminé<br />";
                }
            }

            if (isset($item['img'])) {
                $res = $res . "<button type=\"text\"> <a href=\"../../suppressionImageItem/$id\">Supprimer image</a></button><br />";
            } else {
                $res = $res . "<br />";
            }

        }

        // si createur + item reservé + apres echeance
        if (isset($_COOKIE['createurListe' . $this->liste['no']]) && (time() - strtotime(Liste::find($_SESSION['idL'])['expiration']) > 0)) {
            if (isset($item['participant'])) {
                $res = $res . "réservé par $participant";
                $res = $res . "<p>MESSAGE : $descrReserv</p>";
            }else{
                if (isset($item['cagnotte_id'])){
                    $c = Cagnotte::find($item['cagnotte_id']);
                    if ($c->prixR > 0) {
                        $res = $res . "cagnotte non terminié<br />";
                    }else{
                        $res = $res . "cagnotte terminié<br />";
                    }
                    $res = $res . "A participé :<br />";
                    foreach ($c->participes as $part){
                        $u = User::find($part->id_user);
                        $res = $res . "- ". $u->nom . " " . $u->prenom . " avec " . $part->aPaye . " euro<br />";
                    }
                }else{
                    $res = $res . "<p>Le produit n'a pas était commandé</p>";
                }
            }

        }

        // si pas createur liste + item reservé
        if ((! isset($_COOKIE['createurListe' . $this->liste['no']])) && isset($item['participant'])) {
            $res = $res . "réservé par $participant";
        }

        // si pas reservé + avant echeance
        if ((! isset($_COOKIE['createurListe' . $this->liste['no']])) && (! isset($item['participant'])) && (strtotime(Liste::find($_SESSION['idL'])['expiration']) - time() > 0)) {
            if (isset($item->cagnotte_id) && Cagnotte::find($item->cagnotte_id)->prixR > 0) {
                $prixC = Cagnotte::find($item->cagnotte_id);
                $prixC = $prixC->prixR;
                $res = $res . "<p><b>Une cagnotte a etait ouvert pour cette objet</b></p>";
                $res = $res . "<p><b>Il reste . $prixC . à complète</b></p>";
                if (isset($_SESSION['iduser'])) {
                    $res = $res . $this->afficherFormParticipe($id);
                } else {
                    $res = $res . "<p><b>Vous devez etre connecté pour participer</b></p>";
                }
            } else {
                if (isset($item->cagnotte_id)) {
                    $res = $res . "<p><b>Cagnotte complete</b></p>";
                } else {
                    $res = $res . "<p><b>PAS ENCORE RÉSERVÉ</b></p>";
                    $res = $res . "<button type=\"text\"> <a href=\"../../formuReserverItem/$id\">Réserver cet item</a></button><br />";

                }
            }
		}
		
		// si pas reservé + apres echeance
        if (! isset($_COOKIE['createurListe' . $this->liste['no']]) && (! isset($item['participant'])) && (time() - strtotime(Liste::find($_SESSION['idL'])['expiration']) > 0)) {
            $res = $res . "<p>Le produit n'a pas était commandé</p>";
        }


        
        return $res . "</div>";
    }
	
	public function afficherFormParticipe($id)
    {
        $d = $_SESSION['idL'];
        if (isset($_SESSION['iduser'])) {
            $v = User::find($_SESSION['iduser'])['username'];
        } else {
            $v = '';
        }
        $res = <<<END
            <div class="formulaireNewParticp">
                <h4>Complete la cagnotte :</h4>
                <form method='post' action='$id/creeParticipe/' enctype="multipart/form-data">
                    <label for="prixP"> <b>Completer de :*</b></label><br />
                    <input type="number" name="prixP" min="0" step="0.01" required>
                    <button type="submit" value="Valider"> Valider</button>
                    <p>* champs requis</p>
                </form>
            </div>
END;
        return $res;
    }

    /**
     * Affiche le formulaire de la modification d'un item
     *
     * @return string
     */
    public function afficherFormuModificationItem()
    {
        $idI = $_SESSION['idItem'];
        $item = Item::find($idI);
        $cagnotte ='';

        if (isset($item->cagnotte_id)){
            if (! isset(Cagnotte::find($item->cagnotte_id)->first()->participes)){
                $cagnotte ='<td>Fermer une cagnotte </td><td><input type = "checkbox" name =Fcagnotte></td>';
            }
        }else{
            if (! isset($item->participant)){
                $cagnotte ='<td>Ouvrir une cagnotte </td><td><input type = "checkbox" name =Ocagnotte></td>';
            }
        }
		$res = <<<END
        <div class="formulaireModifItem">
            <h2>MODIFICATION D'UN ITEM</h2>
            <div class="text-input">
                <form method="post" action="../modifierItemPourListe/$idI" enctype="multipart/form-data\>
                    <label for="nom_item"> <b>NOM DE L'ITEM :</b></label>
                    <input type="text" name="nom_item" id="nom_item" placeholder="Exemple : Champagne" size="30" maxlength="30" autofocus>
                        
                    <label for="prix_item"> <b>PRIX :</b></label>
                    <input type="number" name="prix_item" id="prix_item" placeholder="00.00" min="0" step="0.01">
                    <br /><br />
                        
                    <label for="desc_item"> <b>DESCRIPTION DE L'ITEM :</b></label> <br />
                    <textarea name="descr" id="descr" rows="4" cols="60" placeholder="Exemple : Bouteille de champagne + flûtes"></textarea>
                    <br />

                    <label for="img_item"> <b>Image de l'item :</b></label>
                    <input type="file" name="img_itemM" id="img" accept="image/*">
                    <br />
                        
                    <label for="urlExt_item"> URL externe détails item :</label>
                    <input type="url" name="urlExt_item" id="url" size="80" maxlength="1000">
                    <br />
					
					$cagnotte
                        
                    <input type="submit" id="btValide" value="Valider">
                </form>
            </div>
        </div>
END;
        return $res;
    }

    // public function partageListe(){
    // $res = <<<END
    // <div>
    // <p>URL de votre liste</p>
    // </div>
    // END;
    
    // return $res;
    // }
    
    /**
     *
     * @param int $sel
     */
    public function render($sel)
    {
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request->getRootUri();
        $css = "$rootUri/src/vues/fileCSS.css";
        $lienAccueil = "$rootUri/listes";
        $lienCreateurs = "$rootUri/lesCreateurs";
        $lienDeco = "$rootUri/deconnection";
        $lienNvListe = "$rootUri/formuCreerListe";
        $lienConnection = "$rootUri/connection";
        $lienInscription = "$rootUri/inscription";
        $lienProfil = "$rootUri/afficheProfil";
//         $css = "../../src/vues/fileCSS.css";
//         $lienAccueil = "../../listes";
//         $lienNvListe = "../../formuCreerListe";
//         $lienConnection = "../../connection";
//         $lienInscription = "../../inscription";
//         $lienNouvelleListe = "../../formuCreerListe";
//         $lienCreateurs = "../../lesCreateurs";
//         $lienProfil = "../../afficheProfil";
        switch ($sel) {
            case 1:
                $content = $this->afficherItems();
                
                break;
            case 2:
                $content = $this->afficherFormuModificationItem();
//                 $css = "../src/vues/fileCSS.css";
//                 $lienAccueil = "../listes";
//                 $lienNvListe = "../formuCreerListe";
//                 $lienCreateurs = "../lesCreateurs";
                break;
        }
        
        if (isset($_SESSION['iduser'])) {
//             if (isset($_SESSION['idL'])) {
//                 $lienDeco = "../../deconnection";
//             } else {
//                 $lienDeco = "../deconnection";
//             }
            $connectBouton = "<li class=\"nav-item\"><a class=\"nav-link\" href=$lienDeco>DÉCONNEXION</a></li>";
            $espacePerso = "<li class=\"nav-item\"><a class=\"nav-link\" href=\"$lienProfil\">MON ESPACE </a></li>";
        } else {
            $connectBouton = "<li class=\"nav-item\"><a class=\"nav-link\" href= $lienConnection>CONNEXION</a></li> <li class=\"nav-item\"> <a class=\"nav-link\" href=$lienInscription>INSCRIPTION</a></li>";
            $espacePerso = "";
        }
        
        $creaListe = '';
        if (isset($_SESSION['iduser'])) {
            $creaListe = "<li class=\"nav-item\"> <a class=\"nav-link\" href=\"$lienNvListe\">NOUVELLE LISTE</a></li>";
        }
        
        $html = <<<END
   <!DOCTYPE html>
   <html lang="fr">
   <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="$css">
        <title>My WishList</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="$lienAccueil">MyWishList</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="$lienAccueil">ACCUEIL <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="$lienCreateurs">LES CRÉATEURS</a>
                </li>
                $creaListe
                $espacePerso
                $connectBouton 
              </ul>
            </div>
          </nav>
          <br /><br /><br />
            $content
            
	       <footer>
                <hr>
                <p>Projet PHP - My WishList </p>
                <p>KIRCHER-LECLERC-MARTIGNON-MAYER</p>
                <p><a href="https://bitbucket.org/mayer66u/php_projet_2018_2019/src/master/" target="_blank">Cliquez pour voir notre dépôt GIT</a></p>
    		  </footer>
        </div>
    </body>
    </html>

END;
        
        echo $html;
    }
}