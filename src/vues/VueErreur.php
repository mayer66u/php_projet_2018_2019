<?php
/**
 * @author Leclerc Oscar, Kircher Nicolas, Martignon Thomas et Mayer Théo
 * Projet PHP - My WishList 
 */
namespace wishlist\vues;

class VueErreur
{

    public static function render($err)
    {
        $i = $_SESSION['connectStatus'];
        
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request->getRootUri();
        $css = "$rootUri/src/vues/fileCSS.css";
        $lienAccueil = "$rootUri/listes";
        $lienCreateurs = "$rootUri/lesCreateurs";
        
        $html = <<<END
        <!DOCTYPE html>
            <html lang="fr">
            <head>
                <meta charset="utf-8" />
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script> 
                <link rel="stylesheet" href="$css">
                <title>My WishList</title>  
                </head>
    
                <body>
                    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                        <a class="navbar-brand" href="$lienAccueil">MyWishList</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse" id="navbarCollapse">
                          <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                              <a class="nav-link" href="$lienAccueil">ACCUEIL <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="$lienCreateurs">LES CRÉATEURS</a>
                            </li>
                          </ul>
                        </div>
                    </nav>
                    <div class="corps">
                        $err
                    </div>
                    <footer>
                        <hr>
                        <p>Projet PHP - My WishList </p>
                        <p>KIRCHER-LECLERC-MARTIGNON-MAYER</p>
                        <p><a href="https://bitbucket.org/mayer66u/php_projet_2018_2019/src/master/" target="_blank">Cliquez pour voir notre dépôt GIT</a></p> 
            		    </footer>
                </body>
            </html>
END;
        
        echo $html;
    }
}